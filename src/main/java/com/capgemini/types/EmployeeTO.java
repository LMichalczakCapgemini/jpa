package com.capgemini.types;

import com.capgemini.domain.PersonalData;
import com.capgemini.enumeration.Post;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EmployeeTO {
    private Long id;

    private Post post;

    private PersonalData personalData;

    private List<CarTO> cars;

    private AgencyTO agency;

    public EmployeeTO() {
    }

    public EmployeeTO(EmployeeTOBuilder builder) {
        this.id = builder.id;
        this.post = builder.post;
        this.personalData = builder.personalData;
        this.cars = builder.cars;
        this.agency = builder.agency;
    }

    public Long getId() {
        return id;
    }

    public Post getPost() {
        return post;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public List<CarTO> getCars() {
        return cars;
    }

    public void setCars(List<CarTO> cars) {
        this.cars = cars;
    }

    public AgencyTO getAgency() {
        return agency;
    }

    public void setAgency(AgencyTO agency) {
        this.agency = agency;
    }

    public static class EmployeeTOBuilder{
        private Long id;

        private Post post;

        private PersonalData personalData;

        private List<CarTO> cars = new ArrayList<>();

        private AgencyTO agency;

        public Long getId() {
            return id;
        }

        public Post getPost() {
            return post;
        }

        public PersonalData getPersonalData() {
            return personalData;
        }

        public List<CarTO> getCars() {
            return cars;
        }

        public EmployeeTOBuilder withId(Long id){
            this.id = id;
            return this;
        }

        public EmployeeTOBuilder withPost(Post post){
            this.post = post;
            return this;
        }

        public EmployeeTOBuilder withPersonalData(PersonalData personalData){
            this.personalData = personalData;
            return this;
        }

        public EmployeeTOBuilder withCar(CarTO carTO){
            this.cars.add(carTO);
            return this;
        }

        public EmployeeTOBuilder withCars(List<CarTO> carTOs){
            this.cars.addAll(carTOs);
            return this;
        }

        public EmployeeTOBuilder withAgency(AgencyTO agency){
           this.agency = agency;
           return this;
        }

        public EmployeeTO build(){
            return new EmployeeTO(this);
        }
    }

}
