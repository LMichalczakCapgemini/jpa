package com.capgemini.types;

import com.capgemini.domain.AddressData;
import com.capgemini.domain.ContactData;

import java.util.LinkedList;
import java.util.List;

public class AgencyTO {
    private Long id;

    private AddressData addressData;

    private ContactData contactData;

    private List<RentTO> takeRents;

    private List<RentTO> returnRents;

    public Long getId() {
        return id;
    }

    public AddressData getAddressData() {
        return addressData;
    }

    public ContactData getContactData() {
        return contactData;
    }

    public List<RentTO> getTakeRents() {
        return takeRents;
    }

    public List<RentTO> getReturnRents() {
        return returnRents;
    }

    public AgencyTO() {
    }



    public AgencyTO(AgencyTOBuilder builder) {
        this.id = builder.id;
        this.addressData = builder.addressData;
        this.contactData = builder.contactData;
        this.takeRents = builder.takeRents;
        this.returnRents = builder.returnRents;
    }

    public static class AgencyTOBuilder{
        private Long id;

        private AddressData addressData;

        private ContactData contactData;

        private List<RentTO> takeRents = new LinkedList<>();

        private List<RentTO> returnRents = new LinkedList<>();

        public AgencyTOBuilder withId(Long id){
            this.id = id;
            return this;
        }

        public AgencyTOBuilder withContactData(ContactData contactData){
            this.contactData = contactData;
            return this;
        }

        public AgencyTOBuilder withAddressData(AddressData addressData){
            this.addressData = addressData;
            return this;
        }

        public AgencyTOBuilder withTakeRent(RentTO rentTO){
            this.takeRents.add(rentTO);
            return this;
        }

        public AgencyTOBuilder withTakeRents(List<RentTO> rentTOs){
            this.takeRents.addAll(rentTOs);
            return this;
        }

        public AgencyTOBuilder withRetutnRent(RentTO rentTO){
            this.returnRents.add(rentTO);
            return this;
        }

        public AgencyTOBuilder withReturnRents(List<RentTO> rentTOs){
            this.returnRents.addAll(rentTOs);
            return this;
        }

        public Long getId() {
            return id;
        }

        public AddressData getAddressData() {
            return addressData;
        }

        public ContactData getContactData() {
            return contactData;
        }

        public List<RentTO> getTakeRents() {
            return takeRents;
        }

        public List<RentTO> getReturnRents() {
            return returnRents;
        }

        public AgencyTO build(){
            return new AgencyTO(this);
        }
    }
}
