package com.capgemini.types;

import com.capgemini.enumeration.CarType;

import java.time.Year;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class CarTO {
    private Long id;

    private CarType carType;

    private String brand;

    private Year productionYear;

    private String color;

    private Double engineCapacity;

    private int horsePower;

    private double millageKm;

    private List<RentTO> rents;

    public CarTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Year getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Year productionYear) {
        this.productionYear = productionYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(Double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public double getMillageKm() {
        return millageKm;
    }

    public void setMillageKm(int millageKm) {
        this.millageKm = millageKm;
    }

    public List<RentTO> getRents() {
        return rents;
    }

    public void setRents(List<RentTO> rents) {
        this.rents = rents;
    }

    public CarTO(CarTOBuilder builder) {
        this.id = builder.id;
        this.carType = builder.carType;
        this.brand = builder.brand;
        this.productionYear = builder.productionYear;
        this.color = builder.color;
        this.engineCapacity = builder.engineCapacity;
        this.horsePower = builder.horsePower;
        this.millageKm = builder.millageKm;
        this.rents = builder.rents;
    }

    public static class CarTOBuilder{
        private Long id;

        private CarType carType;

        private String brand;

        private Year productionYear;

        private String color;

        private Double engineCapacity;

        private int horsePower;

        private double millageKm;

        private List<RentTO> rents = new LinkedList<>();

        public CarTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public CarTOBuilder withCarType(CarType carType) {
            this.carType = carType;
            return this;
        }

        public CarTOBuilder withBrand(String brand) {
            this.brand = brand;
            return this;
        }

        public CarTOBuilder withProductYear(Year year) {
            this.productionYear = year;
            return this;
        }

        public CarTOBuilder withColor(String color) {
            this.color = color;
            return this;
        }

        public CarTOBuilder withEngCap(double engineCapacity) {
            this.engineCapacity = engineCapacity;
            return this;
        }

        public CarTOBuilder withHorsePower(int horsePower) {
            this.horsePower = horsePower;
            return this;
        }

        public CarTOBuilder withMillage(double millageKm) {
            this.millageKm = millageKm;
            return this;
        }

        public CarTOBuilder withRents(List<RentTO> rents) {
            this.rents.addAll(rents);
            return this;
        }

        public CarTOBuilder withRent(RentTO rent) {
            this.rents.add(rent);
            return this;
        }

        public CarTO build(){
            return new CarTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarTO)) return false;
        CarTO carTO = (CarTO) o;
        return getHorsePower() == carTO.getHorsePower() &&
                Double.compare(carTO.getMillageKm(), getMillageKm()) == 0 &&
                Objects.equals(getId(), carTO.getId()) &&
                getCarType() == carTO.getCarType() &&
                Objects.equals(getBrand(), carTO.getBrand()) &&
                Objects.equals(getProductionYear(), carTO.getProductionYear()) &&
                Objects.equals(getColor(), carTO.getColor()) &&
                Objects.equals(getEngineCapacity(), carTO.getEngineCapacity()) &&
                Objects.equals(getRents(), carTO.getRents());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCarType(), getBrand(), getProductionYear(), getColor(), getEngineCapacity(), getHorsePower(), getMillageKm(), getRents());
    }
}
