package com.capgemini.types;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class RentTO {
    private Long id;

    private LocalDate takeDate;

    private LocalDate returnDate;

    private double cost;

    public RentTO() {
    }

    public RentTO(RentToBuilder builder){
        this.id = builder.id;
        this.takeDate = builder.takeDate;
        this.returnDate = builder.returnDate;
        this.cost = builder.cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTakeDate() {
        return takeDate;
    }

    public void setTakeDate(LocalDate takeDate) {
        this.takeDate = takeDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public static class RentToBuilder{
        private Long id;

        private LocalDate takeDate;

        private LocalDate returnDate;

        private double cost;

        public RentToBuilder withId(Long id){
            this.id = id;
            return this;
        }

        public RentToBuilder withTakeDate(LocalDate date){
            this.takeDate = date;
            return this;
        }

        public RentToBuilder withReturnDate(LocalDate date){
            this.returnDate = date;
            return this;
        }

        public RentToBuilder withCost(double cost){
            this.cost = cost;
            return this;
        }

        public RentTO build(){
            return new RentTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RentTO)) return false;
        RentTO rentTO = (RentTO) o;
        return Double.compare(rentTO.getCost(), getCost()) == 0 &&
                Objects.equals(getId(), rentTO.getId()) &&
                Objects.equals(getTakeDate(), rentTO.getTakeDate()) &&
                Objects.equals(getReturnDate(), rentTO.getReturnDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTakeDate(), getReturnDate(), getCost());
    }
}
