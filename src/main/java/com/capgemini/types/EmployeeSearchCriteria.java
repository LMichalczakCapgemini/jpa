package com.capgemini.types;

import com.capgemini.enumeration.Post;

public class EmployeeSearchCriteria {
    private Long agencyId;
    private Long carId;
    private Post post;

    public EmployeeSearchCriteria() {
    }

    public EmployeeSearchCriteria(Long agencyId, Long carId, Post post) {
        this.agencyId = agencyId;
        this.carId = carId;
        this.post = post;
    }

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
