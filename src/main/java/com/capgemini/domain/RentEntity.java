package com.capgemini.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "RENT")
public class RentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(columnDefinition = "DATE", nullable = false)
    private LocalDate takeDate;

    @Column(columnDefinition = "DATE", nullable = false)
    private LocalDate returnDate;

    @Column(nullable = false, columnDefinition = "DECIMAL", precision = 2, scale = 6)
    private double cost;

    public RentEntity() {
    }

    public RentEntity(LocalDate takeDate, LocalDate returnDate, double cost) {
        this.takeDate = takeDate;
        this.returnDate = returnDate;
        this.cost = cost;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public LocalDate getTakeDate() {
        return takeDate;
    }

    public void setTakeDate(LocalDate takeDate) {
        this.takeDate = takeDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
