package com.capgemini.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Embeddable
public class PersonalData {
    @Column(length = 30, nullable = false)
    private String firstName;

    @Column(length = 30, nullable = false)
    private String lastName;

    @Column(columnDefinition = "DATE", nullable = false)
    private LocalDate birthDate;

    public PersonalData() {
    }

    public PersonalData(String firstName, String lastName, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
}