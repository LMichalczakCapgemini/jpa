package com.capgemini.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "AGENCY")
public class AgencyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Embedded
    private AddressData addressData;

    @Embedded
    private ContactData contactData;

    @OneToMany
    @JoinColumn(name = "TAKE_AGENCY_ID")
    private List<RentEntity> takeRents;

    @OneToMany
    @JoinColumn(name = "RETURN_AGENCY_ID")
    private List<RentEntity> returnRents;

    public AgencyEntity() {
    }

    public AgencyEntity(AddressData addressData, ContactData contactData,
                        List<RentEntity> takeRents, List<RentEntity> returnRents) {
        this.addressData = addressData;
        this.contactData = contactData;
        this.takeRents = takeRents;
        this.returnRents = returnRents;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public AddressData getAddressData() {
        return addressData;
    }

    public void setAddressData(AddressData addressData) {
        this.addressData = addressData;
    }

    public ContactData getContactData() {
        return contactData;
    }

    public void setContactData(ContactData contactData) {
        this.contactData = contactData;
    }

    public List<RentEntity> getTakeRents() {
        return takeRents;
    }

    public void setTakeRents(List<RentEntity> takeRents) {
        this.takeRents = takeRents;
    }

    public List<RentEntity> getReturnRents() {
        return returnRents;
    }

    public void setReturnRents(List<RentEntity> returnRents) {
        this.returnRents = returnRents;
    }


}
