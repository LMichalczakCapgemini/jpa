package com.capgemini.domain;

import com.capgemini.enumeration.CarType;

import javax.persistence.*;
import java.time.Year;
import java.util.List;

@Entity
@Table(name = "CAR")
public class CarEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private CarType carType;

    @Column(length = 15, nullable = false)
    private String brand;

    @Column(columnDefinition = "YEAR")
    private Year productionYear;

    @Column(length = 15, nullable = false)
    private String color;

    @Column(columnDefinition = "DECIMAL", scale = 4, nullable = false)
    private Double engineCapacity;

    @Column(columnDefinition = "DECIMAL", scale = 4, nullable = false)
    private int horsePower;

    @Column(columnDefinition = "DECIMAL", scale = 7, precision = 1, nullable = false)
    private double millageKm;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CAR_ID")
    private List<RentEntity> rents;

    public CarEntity() {
    }

    public CarEntity(CarType carType, String brand, Year productionYear, String color, Double engineCapacity,
                     int horsePower, double millageKm, List<RentEntity> rents) {
        this.carType = carType;
        this.brand = brand;
        this.productionYear = productionYear;
        this.color = color;
        this.engineCapacity = engineCapacity;
        this.horsePower = horsePower;
        this.millageKm = millageKm;
        this.rents = rents;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Year getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Year productionYear) {
        this.productionYear = productionYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(Double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public double getMillageKm() {
        return millageKm;
    }

    public void setMillageKm(double millageKm) {
        this.millageKm = millageKm;
    }

    public List<RentEntity> getRents() {
        return rents;
    }

    public void setRents(List<RentEntity> rents) {
        this.rents = rents;
    }
}
