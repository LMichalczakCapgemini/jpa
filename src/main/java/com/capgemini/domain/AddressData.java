package com.capgemini.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AddressData {
    @Column(length = 60, nullable = false)
    private String city;

    @Column(length = 60)
    private String street;

    @Column(length = 20, nullable = false)
    private String localNumber;

    public AddressData() {
    }

    public AddressData(String city, String street, String localNumber) {
        this.city = city;
        this.street = street;
        this.localNumber = localNumber;
    }
}
