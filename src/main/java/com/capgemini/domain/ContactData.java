package com.capgemini.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ContactData {
    @Column(length = 15)
    private String phoneNumber;

    @Column(length = 60)
    private String email;

    public ContactData() {
    }

    public ContactData(String phoneNumber, String email) {
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
}
