package com.capgemini.domain;

import javax.persistence.*;
import java.util.Collection;
import java.util.LinkedList;

@Entity
@Table(name = "Customer")
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, columnDefinition = "DECIMAL", scale = 16)
    private String creditCard;

    @Embedded
    private AddressData addressData;

    @Embedded
    private ContactData contactData;

    @Embedded
    private PersonalData personalData;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CUSTOMER_ID")
    private Collection<RentEntity> rents = new LinkedList<>();

    public CustomerEntity() {
    }

    public CustomerEntity(String creditCard, AddressData addressData, ContactData contactData,
                          PersonalData personalData, Collection<RentEntity> rents) {
        this.creditCard = creditCard;
        this.addressData = addressData;
        this.contactData = contactData;
        this.personalData = personalData;
        this.rents = rents;
    }
}
