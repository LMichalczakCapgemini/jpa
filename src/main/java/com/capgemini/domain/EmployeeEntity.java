package com.capgemini.domain;

import com.capgemini.enumeration.Post;
import com.capgemini.types.AgencyTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "EMPLOYEE")
public class EmployeeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Post post;

    @Embedded
    private PersonalData personalData;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "CAR_EMPLOYEE",
            joinColumns = { @JoinColumn(name = "EMPLOYEE_ID", nullable = false, updatable = false)},
            inverseJoinColumns = { @JoinColumn(name = "CAR_ID", nullable = false, updatable = false)})
    private List<CarEntity> cars = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="Agency_ID")
    private AgencyEntity agency;

    public EmployeeEntity() {
    }

    public EmployeeEntity(Post post, PersonalData personalData, List<CarEntity> cars, AgencyEntity agency) {
        this.post = post;
        this.personalData = personalData;
        this.cars = cars;
        this.agency = agency;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }

    public AgencyEntity getAgency() {
        return agency;
    }

    public void setAgency(AgencyEntity agency) {
        this.agency = agency;
    }
}
