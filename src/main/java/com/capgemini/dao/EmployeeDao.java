package com.capgemini.dao;

import com.capgemini.dao.Dao;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.types.EmployeeSearchCriteria;

import java.util.List;

public interface EmployeeDao extends Dao<EmployeeEntity, Long> {
    void addEmployeeToAgency(Long agencyId, Long employeeId);
    List<EmployeeEntity> findAllEmployeesOfAgency(Long agencyId);
    List<EmployeeEntity> findAllKeepersOfCarInAgency(Long agencyId, Long carId);
    List<EmployeeEntity> findBySearchCriteria(EmployeeSearchCriteria searchCriteria);
}
