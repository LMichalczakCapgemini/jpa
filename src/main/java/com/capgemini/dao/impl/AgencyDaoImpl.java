package com.capgemini.dao.impl;

import com.capgemini.dao.AgencyDao;
import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.EmployeeEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class AgencyDaoImpl extends AbstractDao<AgencyEntity, Long> implements AgencyDao {
    @Override
    public List<EmployeeEntity> findAllEmployeesOfAgency(Long agencyId) {
        TypedQuery<EmployeeEntity> query = entityManager.createQuery(
                "select employees from AgencyEntity agency where agency.id = :id", EmployeeEntity.class);
        query.setParameter("id", agencyId);
        return query.getResultList();
    }

    @Override
    public List<EmployeeEntity> findEmployerOfAgencyByKeepengCar(Long agencyId, Long carId) {
        TypedQuery<EmployeeEntity> query = entityManager.createQuery(
                "select e from EmployeeEntity e join e.cars c join where e.cars.contains(:carId)" +
                        "and a.id = :agencyId", EmployeeEntity.class);
        query.setParameter("agencyId", agencyId);
        query.setParameter("carId", carId);
        return query.getResultList();
    }

    @Override
    public void addEmployeeToAgencyQuery(Long agencyId, Long employeeId){
        Query query = entityManager.createQuery(
                "update EmployeeEntity e set e.agency.id = :agencyId where e.id = :employeeId");
        query.setParameter("agencyId", agencyId);
        query.setParameter("employeeId", employeeId);
    }

    @Override
    public void removeEmployeeFromAgencyQuery(Long agencyId, Long employeeId){
        Query query = entityManager.createQuery(
                "delete from AgencyEntity a join a.employees e");
        query.setParameter("agencyId", agencyId);
        query.setParameter("employeeId", employeeId);
    }
}
