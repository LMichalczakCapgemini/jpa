package com.capgemini.dao.impl;

import com.capgemini.dao.RentDao;
import com.capgemini.domain.RentEntity;
import org.springframework.stereotype.Repository;

@Repository
public class RentDaoImpl extends AbstractDao<RentEntity, Long> implements RentDao {
}
