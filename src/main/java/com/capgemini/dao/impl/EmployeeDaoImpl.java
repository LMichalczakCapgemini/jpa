package com.capgemini.dao.impl;

import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.enumeration.Post;
import com.capgemini.types.EmployeeSearchCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Repository
public class EmployeeDaoImpl extends AbstractDao<EmployeeEntity, Long> implements EmployeeDao {
    @Override
    public void addEmployeeToAgency(Long agencyId, Long employeeId) {
        Query query = entityManager.createQuery(
                "update EmployeeEntity e set e.agency");
        query.setParameter("agencyId", agencyId);
        query.setParameter("employeeId", employeeId);
    }

    @Override
    public List<EmployeeEntity> findAllEmployeesOfAgency(Long agencyId) {
        TypedQuery<EmployeeEntity> query = entityManager.createQuery(
                "select e from EmployeeEntity e where e.agency.id = :agencyId", EmployeeEntity.class);
        query.setParameter("agencyId", agencyId);
        return query.getResultList();
    }

    @Override
    public List<EmployeeEntity> findAllKeepersOfCarInAgency(Long agencyId, Long carId) {
        TypedQuery<EmployeeEntity> query = entityManager.createQuery(
                "select e from EmployeeEntity e where e.agency.id = :agencyId and (select c from CarEntity c where c.id = :carId) member of e.cars", EmployeeEntity.class);
        query.setParameter("agencyId", agencyId);
        query.setParameter("carId", carId);
        return query.getResultList();
    }

    @Override
    public List<EmployeeEntity> findBySearchCriteria(EmployeeSearchCriteria searchCriteria) {
        Post searchPost = searchCriteria.getPost();
        Long searchAgencyId = searchCriteria.getAgencyId();
        Long searchCarId = searchCriteria.getCarId();

        String paramAgencyId = searchAgencyId != null ? searchAgencyId.toString() : "e.agency.id";
        String paramCarId = searchCarId != null ? searchCarId.toString() : "c.id";

        TypedQuery<EmployeeEntity> query = entityManager.createQuery(
                "select e from EmployeeEntity e join e.cars c where e.post IN (:postType) and e.agency.id = " + paramAgencyId + " " +
                        "and c.id = " + paramCarId + "", EmployeeEntity.class);
        query.setParameter("postType", searchPost != null ? searchPost : new ArrayList<>(EnumSet.allOf(Post.class)));
        return query.getResultList();
    }
}
