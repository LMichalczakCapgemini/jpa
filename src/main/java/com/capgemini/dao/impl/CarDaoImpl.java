package com.capgemini.dao.impl;

import com.capgemini.dao.CarDao;
import com.capgemini.domain.CarEntity;
import com.capgemini.enumeration.CarType;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Repository
public class CarDaoImpl extends AbstractDao<CarEntity, Long> implements CarDao {
    @Override
    public List<CarEntity> findCarByBrand(String brand) {
        TypedQuery<CarEntity> query = entityManager.createQuery(
                "select c from CarEntity c where c.brand like :brand", CarEntity.class);
        query.setParameter("brand", brand != null ? brand : "%");
        return query.getResultList();
    }

    @Override
    public List<CarEntity> findCarByCarType(CarType carType) {
        TypedQuery<CarEntity> query = entityManager.createQuery(
                "select c from CarEntity c where c.carType IN (:carTypeList)", CarEntity.class);
        query.setParameter("carTypeList", carType != null ? carType : new ArrayList<>(EnumSet.allOf(CarType.class)));
        return query.getResultList();
    }

    @Override
    public List<CarEntity> findCarByCarTypeAndBrand(CarType carType, String brand) {
        TypedQuery<CarEntity> query = entityManager.createQuery(
                "select c from CarEntity c where c.carType IN (:carTypeList) and c.brand like :brand", CarEntity.class);
        query.setParameter("carTypeList", carType != null ? carType : new ArrayList<>(EnumSet.allOf(CarType.class)));
        query.setParameter("brand", brand != null ? brand : "%");
        return query.getResultList();
    }

    @Override
    public List<CarEntity> findCarsRentMoreThanXCustomers(Long x) {
        TypedQuery<CarEntity> query = entityManager.createQuery(
                "select car from CarEntity car " +
                        "join car.rents carRent " +
                        "join CustomerEntity customer on carRent member of customer.rents " +
                        "group by car having count(distinct customer) > :x", CarEntity.class);
        query.setParameter("x", x);
        return query.getResultList();
    }

    @Override
    public Long countUsedCarsInDateRange(LocalDate from, LocalDate to) {
        TypedQuery<Long> query = entityManager.createQuery(
              "select count(distinct car.id) from CarEntity car join car.rents carRent " +
                      "where carRent.takeDate < :dateTo and carRent.returnDate > :dateFrom " , Long.class);
        query.setParameter("dateFrom", from);
        query.setParameter("dateTo", to);
        return query.getSingleResult();
    }

}