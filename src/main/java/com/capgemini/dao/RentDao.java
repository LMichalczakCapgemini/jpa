package com.capgemini.dao;

import com.capgemini.dao.Dao;
import com.capgemini.domain.RentEntity;

public interface RentDao extends Dao<RentEntity, Long> {
}
