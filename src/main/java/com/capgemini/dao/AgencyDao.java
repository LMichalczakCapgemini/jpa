package com.capgemini.dao;

import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.EmployeeEntity;

import java.util.List;

public interface AgencyDao extends Dao<AgencyEntity, Long> {
    List<EmployeeEntity> findAllEmployeesOfAgency(Long agencyId);
    List<EmployeeEntity> findEmployerOfAgencyByKeepengCar(Long agencyId, Long carId);
    void addEmployeeToAgencyQuery(Long agencyId, Long employeeId);
    void removeEmployeeFromAgencyQuery(Long agencyId, Long employeeId);
}
