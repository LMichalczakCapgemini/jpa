package com.capgemini.dao;

import com.capgemini.domain.CarEntity;
import com.capgemini.enumeration.CarType;

import java.time.LocalDate;
import java.util.List;

public interface CarDao extends Dao<CarEntity, Long>{
    List<CarEntity> findCarByBrand(String brand);
    List<CarEntity> findCarByCarType(CarType carType);
    List<CarEntity> findCarByCarTypeAndBrand(CarType carType, String brand);
    List<CarEntity> findCarsRentMoreThanXCustomers(Long x);
    Long countUsedCarsInDateRange(LocalDate from, LocalDate to);
}
