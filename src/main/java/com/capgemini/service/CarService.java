package com.capgemini.service;

import com.capgemini.enumeration.CarType;
import com.capgemini.types.CarTO;

import java.time.LocalDate;
import java.util.List;

public interface CarService {
    CarTO findCarById(Long id);

    CarTO saveCar(CarTO carTO);

    void deleteCarById(Long id);

    CarTO updateCar(CarTO carTO);

    void assignToEmployee(Long carId, Long employeeId);

    List<CarTO> findByEmployee(Long employeeId);

    List<CarTO> findByTypeAndBrand(CarType carType, String brand);

    List<CarTO> findCarsRentMoreThanXCustomers(Long x);

    Long countUsedCarsInDateRange(LocalDate from, LocalDate to);
}
