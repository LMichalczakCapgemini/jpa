package com.capgemini.service.impl;

import com.capgemini.dao.EmployeeDao;
import com.capgemini.mappers.EmployeeMapper;
import com.capgemini.service.EmployeeService;
import com.capgemini.types.EmployeeSearchCriteria;
import com.capgemini.types.EmployeeTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao employeeRepository;

    @Override
    public List<EmployeeTO> findBySearchCriteria(EmployeeSearchCriteria searchCriteria) {
        return EmployeeMapper.map2TOs(employeeRepository.findBySearchCriteria(searchCriteria));
    }
}
