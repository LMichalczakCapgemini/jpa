package com.capgemini.service.impl;

import com.capgemini.dao.AgencyDao;
import com.capgemini.dao.CarDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.AgencyEntity;
import com.capgemini.exception.FailedAgencyRemoveException;
import com.capgemini.mappers.AgencyMapper;
import com.capgemini.mappers.CarMapper;
import com.capgemini.mappers.EmployeeMapper;
import com.capgemini.service.AgencyService;
import com.capgemini.types.AgencyTO;
import com.capgemini.types.CarTO;
import com.capgemini.types.EmployeeTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class AgencyServiceImpl implements AgencyService {

    @Autowired
    private AgencyDao agencyRepository;

    @Autowired
    private EmployeeDao employeeRepository;

    @Autowired
    private CarDao carRepository;

    @Override
    @Transactional(readOnly = false)
    public AgencyTO saveAgency(AgencyTO agencyTO) {
        AgencyEntity agencyEntity = agencyRepository.save(AgencyMapper.toAgencyEntity(agencyTO));
        return AgencyMapper.toAgencyTo(agencyEntity);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteAgency(Long id) {
        try {
            agencyRepository.delete(id);
        }
        catch (Exception e){
            throw new FailedAgencyRemoveException(e.getMessage());
        }
    }

    @Override
    @Transactional(readOnly = false)
    public AgencyTO updateAgency(AgencyTO agencyTO) {
        AgencyEntity agencyEntity = agencyRepository.update(AgencyMapper.toAgencyEntity(agencyTO));
        return AgencyMapper.toAgencyTo(agencyEntity);
    }

    @Override
    @Transactional(readOnly = false)
    public EmployeeTO addEmployeeToAgency(Long agencyId, Long employeeId) {
        AgencyTO agencyTO = AgencyMapper.toAgencyTo(agencyRepository.findOne(agencyId));
        EmployeeTO employeeTO = EmployeeMapper.toEmployeeTO(employeeRepository.findOne(employeeId));

        employeeTO.setAgency(agencyTO);

        return EmployeeMapper.toEmployeeTO(employeeRepository.update(EmployeeMapper.toEmployeeEntity(employeeTO)));
    }

    @Override
    @Transactional(readOnly = false)
    public EmployeeTO removeEmployeeFromAgency(Long agencyId, Long employeeId) {
        EmployeeTO employeeTO = EmployeeMapper.toEmployeeTO(employeeRepository.findOne(employeeId));
        employeeTO.setAgency(null);

        return EmployeeMapper.toEmployeeTO(employeeRepository.update(EmployeeMapper.toEmployeeEntity(employeeTO)));
    }

    @Override
    public List<EmployeeTO> findAllEmployeesOfAgency(Long agencyId) {
        return EmployeeMapper.map2TOs(employeeRepository.findAllEmployeesOfAgency(agencyId));
    }

    @Override
    public List<EmployeeTO> findKeepersOfCar(Long agencyId, Long carId) {
        List<EmployeeTO> employeeTOs = findAllEmployeesOfAgency(agencyId);
        CarTO carTO = CarMapper.toCarTO(carRepository.findOne(carId));

        return employeeTOs.stream()
                .filter(employeeTO -> employeeTO.getCars().contains(carTO))
                .collect(Collectors.toList());
    }
}
