package com.capgemini.service.impl;

import com.capgemini.dao.CarDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.enumeration.CarType;
import com.capgemini.exception.FailedCarRemoveException;
import com.capgemini.mappers.CarMapper;
import com.capgemini.mappers.EmployeeMapper;
import com.capgemini.service.CarService;
import com.capgemini.types.CarTO;
import com.capgemini.types.EmployeeSearchCriteria;
import com.capgemini.types.EmployeeTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class CarServiceImpl implements CarService {

    @Autowired
    private CarDao carRepository;

    @Autowired
    private EmployeeDao employeeRepository;

    @Override
    public CarTO findCarById(Long id) {
        return CarMapper.toCarTO(carRepository.findOne(id));
    }

    @Override
    @Transactional(readOnly = false)
    public CarTO saveCar(CarTO carTO) {
        CarEntity carEntity = carRepository.save(CarMapper.toCarEntity(carTO));
        return CarMapper.toCarTO(carEntity);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteCarById(Long id){
        try {
            unassignFromKeepers(id);
            carRepository.delete(id);
        }
        catch (Exception e){
            throw new FailedCarRemoveException(e.getMessage());
        }
    }

    private void unassignFromKeepers(Long carId){
        CarTO carTO = CarMapper.toCarTO(carRepository.findOne(carId));
        List<EmployeeTO> employeeTOs = EmployeeMapper.map2TOs(employeeRepository.findAll());
        employeeTOs.stream().filter(employee -> employee.getCars().contains(carTO))
                .forEach(employee ->
                {List<CarTO> cars = employee.getCars();
                cars.remove(carTO);
                employee.setCars(cars);
                employeeRepository.update(EmployeeMapper.toEmployeeEntity(employee));
        });
    }

    @Override
    @Transactional(readOnly = false)
    public CarTO updateCar(CarTO carTO){
        CarEntity carEntity = carRepository.update(CarMapper.toCarEntity(carTO));
        return CarMapper.toCarTO(carEntity);
    }

    @Override
    @Transactional(readOnly = false)
    public void assignToEmployee(Long carId, Long employeeId) {
        EmployeeTO employeeTO = EmployeeMapper.toEmployeeTO(employeeRepository.findOne(employeeId));
        CarTO carTO = CarMapper.toCarTO(carRepository.findOne(carId));

        List<CarTO> carTOs = employeeTO.getCars();
        carTOs.add(carTO);

        employeeTO.setCars(carTOs);

        employeeRepository.update(EmployeeMapper.toEmployeeEntity(employeeTO));
    }

    @Override
    public List<CarTO> findByEmployee(Long employeeId) {
        EmployeeEntity employeeEntity = employeeRepository.findOne(employeeId);
        return CarMapper.map2TOs(employeeEntity.getCars());
    }

    @Override
    public List<CarTO> findByTypeAndBrand(CarType carType, String brand) {
        return CarMapper.map2TOs(carRepository.findCarByCarTypeAndBrand(carType, brand));
    }

    @Override
    public List<CarTO> findCarsRentMoreThanXCustomers(Long x) {
        return CarMapper.map2TOs(carRepository.findCarsRentMoreThanXCustomers(x));
    }

    @Override
    public Long countUsedCarsInDateRange(LocalDate from, LocalDate to) {
        return carRepository.countUsedCarsInDateRange(from, to);
    }
}
