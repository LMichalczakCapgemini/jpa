package com.capgemini.service;

import com.capgemini.types.AgencyTO;
import com.capgemini.types.EmployeeTO;

import java.util.List;

public interface AgencyService {
    AgencyTO saveAgency(AgencyTO agencyTO);

    void deleteAgency(Long id);

    AgencyTO updateAgency(AgencyTO agencyTO);

    EmployeeTO addEmployeeToAgency(Long agencyId, Long employeeId);

    EmployeeTO removeEmployeeFromAgency(Long agencyId, Long employeeId);

    List<EmployeeTO> findAllEmployeesOfAgency(Long agencyId);

    List<EmployeeTO> findKeepersOfCar(Long agencyId, Long carId);
}
