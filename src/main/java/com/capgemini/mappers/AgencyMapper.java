package com.capgemini.mappers;

import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.RentEntity;
import com.capgemini.types.AgencyTO;
import com.capgemini.types.AgencyTO.AgencyTOBuilder;
import com.capgemini.types.RentTO;

import java.util.List;

public class AgencyMapper {
    public static AgencyTO toAgencyTo(AgencyEntity agencyEntity){
        if(agencyEntity == null)
            return null;
        List<RentTO> returnRentsTOs = RentMapper.map2TOs(agencyEntity.getReturnRents());
        List<RentTO> takeRentsTOs = RentMapper.map2TOs(agencyEntity.getTakeRents());
        return new AgencyTOBuilder()
                .withId(agencyEntity.getId())
                .withAddressData(agencyEntity.getAddressData())
                .withContactData(agencyEntity.getContactData())
                .withReturnRents(returnRentsTOs)
                .withTakeRents(takeRentsTOs)
                .build();
    }

    public static AgencyEntity toAgencyEntity(AgencyTO agencyTO){
        if (agencyTO == null)
            return null;
        AgencyEntity agencyEntity = new AgencyEntity();
        agencyEntity.setId(agencyTO.getId());
        agencyEntity.setAddressData(agencyTO.getAddressData());
        agencyEntity.setContactData(agencyTO.getContactData());
        List<RentEntity> returnRentEntities = RentMapper.map2Entities(agencyTO.getReturnRents());
        agencyEntity.setReturnRents(returnRentEntities);
        List<RentEntity> takeRentsEntities = RentMapper.map2Entities(agencyTO.getTakeRents());
        agencyEntity.setTakeRents(takeRentsEntities);
        return agencyEntity;
    }
}
