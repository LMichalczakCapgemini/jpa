package com.capgemini.mappers;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.types.CarTO;
import com.capgemini.types.EmployeeTO;
import com.capgemini.types.EmployeeTO.EmployeeTOBuilder;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeMapper {
    public static EmployeeTO toEmployeeTO(EmployeeEntity employeeEntity){
        if (employeeEntity == null)
            return null;
        List<CarTO> carTOs = CarMapper.map2TOs(employeeEntity.getCars());
        return new EmployeeTOBuilder()
                .withId(employeeEntity.getId())
                .withPersonalData(employeeEntity.getPersonalData())
                .withPost(employeeEntity.getPost())
                .withCars(carTOs)
                .withAgency(AgencyMapper.toAgencyTo(employeeEntity.getAgency()))
                .build();
    }

    public static EmployeeEntity toEmployeeEntity(EmployeeTO employeeTO){
        if (employeeTO == null)
            return null;
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(employeeTO.getId());
        employeeEntity.setPersonalData(employeeTO.getPersonalData());
        employeeEntity.setPost(employeeTO.getPost());
        List<CarEntity> carEntities = CarMapper.map2Entities(employeeTO.getCars());
        employeeEntity.setCars(carEntities);
        employeeEntity.setAgency(AgencyMapper.toAgencyEntity(employeeTO.getAgency()));
        return employeeEntity;
    }

    public static List<EmployeeTO> map2TOs(List<EmployeeEntity> employeeEntities){
        return employeeEntities.stream().map(EmployeeMapper::toEmployeeTO).collect(Collectors.toList());
    }

    public static List<EmployeeEntity> map2Entities(List<EmployeeTO> employeeTOs){
        return employeeTOs.stream().map(EmployeeMapper::toEmployeeEntity).collect(Collectors.toList());
    }
}
