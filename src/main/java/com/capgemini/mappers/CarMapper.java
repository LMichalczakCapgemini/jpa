package com.capgemini.mappers;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.RentEntity;
import com.capgemini.types.CarTO;
import com.capgemini.types.RentTO;

import java.util.List;
import java.util.stream.Collectors;

public class CarMapper {
    public static CarTO toCarTO(CarEntity carEntity){
        if (carEntity == null)
            return null;
        List<RentTO> rentTOs = RentMapper.map2TOs(carEntity.getRents());
        return new CarTO.CarTOBuilder()
                .withId(carEntity.getId())
                .withBrand(carEntity.getBrand())
                .withCarType(carEntity.getCarType())
                .withColor(carEntity.getColor())
                .withEngCap(carEntity.getEngineCapacity())
                .withHorsePower(carEntity.getHorsePower())
                .withMillage(carEntity.getMillageKm())
                .withProductYear(carEntity.getProductionYear())
                .withRents(rentTOs)
                .build();
    }

    public static CarEntity toCarEntity(CarTO carTO){
        if (carTO == null)
            return null;
        CarEntity carEntity = new CarEntity();
        carEntity.setId(carTO.getId());
        carEntity.setBrand(carTO.getBrand());
        carEntity.setCarType(carTO.getCarType());
        carEntity.setColor(carTO.getColor());
        carEntity.setEngineCapacity(carTO.getEngineCapacity());
        carEntity.setHorsePower(carTO.getHorsePower());
        carEntity.setMillageKm(carTO.getMillageKm());
        carEntity.setProductionYear(carTO.getProductionYear());
        List<RentEntity> rents = RentMapper.map2Entities(carTO.getRents());
        carEntity.setRents(rents);
        return carEntity;
    }

    public static List<CarTO> map2TOs(List<CarEntity> carEntities){
        return carEntities.stream().map(CarMapper::toCarTO).collect(Collectors.toList());
    }

    public static List<CarEntity> map2Entities(List<CarTO> carTOs){
        return carTOs.stream().map(CarMapper::toCarEntity).collect(Collectors.toList());
    }
}

