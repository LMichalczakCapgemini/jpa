package com.capgemini.mappers;

import com.capgemini.domain.RentEntity;
import com.capgemini.types.RentTO;

import java.util.List;
import java.util.stream.Collectors;

public class RentMapper {
    public static RentTO toRentTo(RentEntity rentEntity){
        if(rentEntity == null)
            return null;
        return new RentTO.RentToBuilder()
                .withCost(rentEntity.getCost())
                .withId(rentEntity.getId())
                .withReturnDate(rentEntity.getReturnDate())
                .withTakeDate(rentEntity.getTakeDate())
                .build();
    }

    public static RentEntity toRentEntity(RentTO rentTO){
        if (rentTO == null)
            return null;
        RentEntity rentEntity = new RentEntity();
        rentEntity.setId(rentTO.getId());
        rentEntity.setCost(rentTO.getCost());
        rentEntity.setReturnDate(rentTO.getReturnDate());
        rentEntity.setTakeDate(rentTO.getTakeDate());
        return rentEntity;
    }

    public static List<RentTO> map2TOs(List<RentEntity> rentEntities){
        return rentEntities.stream().map(RentMapper::toRentTo).collect(Collectors.toList());
    }

    public static List<RentEntity> map2Entities(List<RentTO> rentTOs){
        return rentTOs.stream().map(RentMapper::toRentEntity).collect(Collectors.toList());
    }
}