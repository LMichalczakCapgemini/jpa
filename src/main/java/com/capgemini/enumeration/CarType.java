package com.capgemini.enumeration;

public enum CarType {
    SEDAN,
    HATCHBACK,
    SUV,
    COUPE
}
