package com.capgemini.exception;

public class FailedCarRemoveException extends RuntimeException {
    public FailedCarRemoveException(String message){
        super(message);
    }
}
