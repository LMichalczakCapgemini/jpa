package com.capgemini.exception;

public class FailedAgencyRemoveException extends RuntimeException {
    public FailedAgencyRemoveException(String message){
        super(message);
    }
}
