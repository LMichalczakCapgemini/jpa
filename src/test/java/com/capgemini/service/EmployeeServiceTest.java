package com.capgemini.service;

import com.capgemini.dao.AgencyDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.*;
import com.capgemini.enumeration.CarType;
import com.capgemini.enumeration.Post;
import com.capgemini.mappers.AgencyMapper;
import com.capgemini.mappers.EmployeeMapper;
import com.capgemini.types.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

import static org.hamcrest.Matchers.*;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
public class EmployeeServiceTest {

    @Autowired
    private EmployeeDao employeeRepository;

    @Autowired
    private AgencyDao agencyRepository;

    @Autowired
    private EmployeeService sut;

    @Test
    @Transactional
    @DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
    public void searchByCriteria(){
        EmployeeSearchCriteria searchCriteria = new EmployeeSearchCriteria(1L,1L, Post.SALESMAN);
        AgencyTO agency = buildAgency();
        CarTO car = buildCar();
        buildEmployees(car,agency);

        List<EmployeeTO> employees = sut.findBySearchCriteria(searchCriteria);

        assertThat(employees.size(),is(equalTo(1)));
    }

    @Test
    @Transactional
    @DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
    public void searchByCriteriaFindNone(){
        EmployeeSearchCriteria searchCriteria = new EmployeeSearchCriteria(1L,1L, Post.ACCAUNTANT);
        AgencyTO agency = buildAgency();
        CarTO car = buildCar();
        buildEmployees(car,agency);

        List<EmployeeTO> employees = sut.findBySearchCriteria(searchCriteria);

        assertThat(employees.isEmpty(),is(equalTo(true)));
    }

    @Test
    @Transactional
    @DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
    public void searchByCriteriaAllNull(){
        EmployeeSearchCriteria searchCriteria = new EmployeeSearchCriteria(null,null, null);
        AgencyTO agency = buildAgency();
        CarTO car = buildCar();
        buildEmployees(car,agency);

        List<EmployeeTO> employees =  sut.findBySearchCriteria(searchCriteria);

        assertThat(employees.size(),is(equalTo(2)));
    }

    private AgencyTO buildAgency(){
        AddressData addressData = new AddressData("Wrocław", "Sztabowa", "12A");
        ContactData contactData = new ContactData("+48123456789", "e.m@email.pl");
        return AgencyMapper.toAgencyTo(agencyRepository.save(AgencyMapper.toAgencyEntity(new AgencyTO.AgencyTOBuilder().withAddressData(addressData).withContactData(contactData)
                .build()) ));
    }

    private void buildEmployees(CarTO carTO, AgencyTO agencyTO){
        PersonalData personalData = new PersonalData("Adam", "Nowak", LocalDate.now());
        PersonalData personalData1 = new PersonalData("Wojtek", "Mały", LocalDate.now());

        EmployeeTO employeeTO = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData)
                .withPost(Post.SALESMAN).withCar(carTO).withAgency(agencyTO).build();
        EmployeeTO employeeTO1 = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData1)
                .withPost(Post.ACCAUNTANT).withCar(carTO).withAgency(agencyTO).build();
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO));
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO1));
    }

    private CarTO buildCar(){
        return new CarTO.CarTOBuilder().withHorsePower(123).withEngCap(1900).withColor("black")
                .withBrand("Opel").withCarType(CarType.COUPE).withMillage(20000).build();
    }

}