package com.capgemini.service;

import com.capgemini.dao.AgencyDao;
import com.capgemini.dao.CarDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.dao.RentDao;
import com.capgemini.domain.*;
import com.capgemini.enumeration.CarType;
import com.capgemini.enumeration.Post;
import com.capgemini.mappers.AgencyMapper;
import com.capgemini.mappers.CarMapper;
import com.capgemini.mappers.EmployeeMapper;
import com.capgemini.mappers.RentMapper;
import com.capgemini.types.AgencyTO;
import com.capgemini.types.AgencyTO.AgencyTOBuilder;
import com.capgemini.types.CarTO;
import com.capgemini.types.EmployeeTO;
import com.capgemini.types.RentTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
public class AgencyServiceTest {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private AgencyDao agencyRepository;

    @Autowired
    private EmployeeDao employeeRepository;

    @Autowired
    private RentDao rentRepository;

    @Autowired
    private CarDao carRepository;

    @Test
    @Transactional
    public void shouldSaveAgency(){
        AgencyTO agencyTO = buildAgency();

        AgencyEntity savedAgency = AgencyMapper.toAgencyEntity(agencyService.saveAgency(agencyTO));

        assertNotNull(savedAgency);
        assertThat(savedAgency.getId(), is(equalTo(1L)));
    }

    @Test
    @Transactional
    public void shouldDeleteAgencyById(){
        AgencyTO agencyTO = buildAgency();
        AgencyTO addedAgencyTO = agencyService.saveAgency(agencyTO);

        agencyService.deleteAgency(addedAgencyTO.getId());
    }


    @Test
    @Transactional
    public void shouldUpdateAgency(){
        AgencyTO agencyTO = buildAgency();
        AgencyTO savedAgency = agencyService.saveAgency(agencyTO);

        AgencyTO updatedAgency = agencyService.updateAgency(savedAgency);

        assertNotNull(updatedAgency);
    }

    @Test
    @Transactional
    public void shouldAddEmployeeToAgency(){
        AgencyTO agencyTO = buildAgency();
        agencyService.saveAgency(agencyTO);
        PersonalData personalData = new PersonalData("Nowy", "Pracownik", LocalDate.now());
        EmployeeTO employeeTO = buildEmployee(personalData);
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO));

        agencyService.addEmployeeToAgency(1l,1L);

        assertThat(employeeRepository.findOne(1L).getAgency().getId(), is(equalTo(1L)));
    }

    @Test
    @Transactional
    public void shouldDeleteEmployeeFromAgency(){
        AgencyTO agencyTO = buildAgency();
        AgencyEntity savedAgency = agencyRepository.save(AgencyMapper.toAgencyEntity(agencyTO));
        buildEmployees(AgencyMapper.toAgencyTo(savedAgency));

        agencyService.removeEmployeeFromAgency(1L,1L);

        assertThat(employeeRepository.findOne(1L).getAgency(), is(equalTo(null)));
        assertThat(employeeRepository.findOne(2L).getAgency().getId(), is(equalTo(1L)));
    }

    @Test
    @Transactional
    public void shouldFindAllEmployeesOfAgency(){
        AgencyTO agencyTO = buildAgency();
        AgencyEntity savedAgency = agencyRepository.save(AgencyMapper.toAgencyEntity(agencyTO));
        buildEmployees(AgencyMapper.toAgencyTo(savedAgency));

        List<EmployeeTO> employees = agencyService.findAllEmployeesOfAgency(1L);

        assertEquals(employees.size(), 2);
        assertEquals(employees.get(0).getPost(), Post.ACCAUNTANT);
    }

    @Test
    @Transactional
    public void shouldFindAllKeepersOfCarInAgency(){
        AgencyTO agencyTO = buildAgency();
        AgencyEntity savedAgency = agencyRepository.save(AgencyMapper.toAgencyEntity(agencyTO));
        CarTO carTO = buildCar();
        buildEmployees(carTO,AgencyMapper.toAgencyTo(savedAgency));

        List<EmployeeTO> carKeepers = agencyService.findKeepersOfCar(1L,2L);

        assertThat(carKeepers.size(),is(equalTo(1)));
        assertThat(agencyService.findAllEmployeesOfAgency(1L).size(), is(equalTo(2)));
    }

    private AgencyTO buildAgency(){
        AddressData addressData = new AddressData("Wrocław", "Sztabowa", "12A");
        ContactData contactData = new ContactData("+48123456789", "e.m@email.pl");
        RentTO rentTO = new RentTO.RentToBuilder().withCost(123.34)
                .withReturnDate(LocalDate.now()).withTakeDate(LocalDate.now()).build();
        return new AgencyTOBuilder().withAddressData(addressData).withContactData(contactData)
                .withRetutnRent(rentTO).withTakeRent(rentTO).build();
    }

    private void buildEmployees(CarTO carTO, AgencyTO agencyTO){
        PersonalData personalData = new PersonalData("Adam", "Nowak", LocalDate.now());
        PersonalData personalData1 = new PersonalData("Wojtek", "Mały", LocalDate.now());

        EmployeeTO employeeTO = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData)
                .withPost(Post.ACCAUNTANT).withCar(carTO).withAgency(agencyTO).build();
        EmployeeTO employeeTO1 = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData1)
                .withPost(Post.ACCAUNTANT).withCar(carTO).withAgency(agencyTO).build();
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO));
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO1));
    }

    private void buildEmployees(AgencyTO agencyTO){
        PersonalData personalData = new PersonalData("Adam", "Nowak", LocalDate.now());
        PersonalData personalData1 = new PersonalData("Wojtek", "Mały", LocalDate.now());

        EmployeeTO employeeTO = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData)
                .withPost(Post.ACCAUNTANT).withAgency(agencyTO).build();
        EmployeeTO employeeTO1 = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData1)
                .withPost(Post.ACCAUNTANT).withAgency(agencyTO).build();
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO));
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO1));
    }

    private EmployeeTO buildEmployee(PersonalData personalData){
        return new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData)
                .withPost(Post.ACCAUNTANT).build();
    }

    private CarTO buildCar(){
        return new CarTO.CarTOBuilder().withHorsePower(123).withEngCap(1900).withColor("black")
                .withBrand("Opel").withCarType(CarType.COUPE).withMillage(20000).build();
    }
}