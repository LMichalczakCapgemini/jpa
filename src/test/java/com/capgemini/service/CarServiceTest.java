package com.capgemini.service;

import com.capgemini.dao.CarDao;
import com.capgemini.dao.CustomerDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.dao.RentDao;
import com.capgemini.domain.*;
import com.capgemini.enumeration.CarType;
import com.capgemini.enumeration.Post;
import com.capgemini.mappers.CarMapper;
import com.capgemini.mappers.EmployeeMapper;
import com.capgemini.mappers.RentMapper;
import com.capgemini.types.CarTO;
import com.capgemini.types.EmployeeTO;
import com.capgemini.types.RentTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
public class CarServiceTest {

    @Autowired
    private CarService sut;

    @Autowired
    private CarDao carRepository;

    @Autowired
    private EmployeeDao employeeRepository;

    @Autowired
    private RentDao rentRepository;

    @Autowired
    private CustomerDao customerRepository;

    @Test
    @Transactional
    public void shouldSaveCar(){
        CarTO carTO = buildCar(CarType.SUV, "Opel");

        CarEntity carEntity = CarMapper.toCarEntity(sut.saveCar(carTO));

        assertThat(carEntity.getId(), is(equalTo(1L)));
        assertThat(carRepository.findAll().size(), is(equalTo(1)));
    }

    @Test
    @Transactional
    public void shouldDeleteCarById(){
        CarTO carTO = buildCar(CarType.SUV, "Opel");
        sut.saveCar(carTO);
        sut.saveCar(carTO);

        sut.deleteCarById(1L);

        assertThat(carRepository.findAll().size(), is(equalTo(1)));
        assertThat(carRepository.findAll().get(0).getId(), is(equalTo(2L)));
    }

    @Test
    @Transactional
    public void shouldUpdateCar(){
        CarTO carTO = buildCar(CarType.SUV, "Opel");
        sut.saveCar(carTO);
        carTO.setId(1L);
        carTO.setColor("yellow");

        CarTO updatedCar = sut.updateCar(carTO);

        assertThat(updatedCar.getColor(),is(equalTo("yellow")));
    }

    @Test
    @Transactional
    public void shouldAssignCarToEmployee(){
        CarTO carTO = buildCar(CarType.SUV, "Opel");
        sut.saveCar(carTO);
        PersonalData personalData = new PersonalData("i", "n", LocalDate.now());
        EmployeeTO employeeTO = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData).withPost(Post.ACCAUNTANT).build();
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO));

        sut.assignToEmployee(1L,1L);

        assertThat(employeeRepository.findOne(1L).getCars().get(0).getId(), is(equalTo(1L)));
        assertThat(employeeRepository.findOne(1L).getCars().get(0).getColor(), is(equalTo("black")));
    }

    @Test
    @Transactional
    public void shouldFindCarByEmployee(){
        CarTO carTO = buildCar(CarType.SUV, "Opel");
        CarTO carTO1 = buildCar(CarType.COUPE, "Audi");
        CarTO carTO2 = buildCar(CarType.SUV, "Renault");
        sut.saveCar(carTO);
        sut.saveCar(carTO1);
        sut.saveCar(carTO2);
        PersonalData personalData = new PersonalData("i", "n", LocalDate.now());
        EmployeeTO employeeTO = new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData).withPost(Post.ACCAUNTANT).build();
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employeeTO));
        sut.assignToEmployee(1L,1L);
        sut.assignToEmployee(2L,1L);

        List<CarTO> employeeCars = sut.findByEmployee(1L);

        assertThat(employeeCars.size(), is(equalTo(2)));
        assertThat(employeeCars.get(0).getId(),is(equalTo(1L)));
        assertThat(employeeCars.get(1).getId(),is(equalTo(2L)));
    }

    @Test
    @Transactional
    public void shouldFindByBrandAndType(){
        CarTO carTO = buildCar(CarType.SUV, "Opel");
        CarTO carTO1 = buildCar(CarType.COUPE, "Opel");
        CarTO carTO2 = buildCar(CarType.SUV, "Audi");
        sut.saveCar(carTO);
        sut.saveCar(carTO1);
        sut.saveCar(carTO2);

        List<CarTO> none = sut.findByTypeAndBrand(CarType.HATCHBACK, "Skoda");
        List<CarTO> brand = sut.findByTypeAndBrand(CarType.SUV, null);
        List<CarTO> type = sut.findByTypeAndBrand(null, "Opel");
        List<CarTO> all = sut.findByTypeAndBrand(null, null);

        assertThat(none.isEmpty(), is(equalTo(true)));
        assertThat(brand.size(),is(equalTo(2)));
        assertThat(type.size(), is(equalTo(2)));
        assertThat(all.size(), is(equalTo(3)));
    }

    @Test
    @Transactional
    public void deleteOfCarShouldRemoveRent(){
        RentTO rentTO = new RentTO.RentToBuilder().withTakeDate(LocalDate.now()).withReturnDate(LocalDate.now())
                .withCost(100).build();
        rentRepository.save(RentMapper.toRentEntity(rentTO));
        List<RentTO> rents = new LinkedList<>();
        rents.add(rentTO);
        rents.add(rentTO);
        CarTO carTO = buildCar(CarType.HATCHBACK, "Opel",rents);
        carRepository.save(CarMapper.toCarEntity(carTO));
        EmployeeTO employee = buildEmployee();
        employeeRepository.save(EmployeeMapper.toEmployeeEntity(employee));
        sut.assignToEmployee(1L, 1L);

        List<RentEntity> rentsBeforeDeleteCar = rentRepository.findAll();
        List<CarEntity> carsBeforeDeleteCar = carRepository.findAll();
        List<EmployeeEntity> employeesBeforeDeleteCar = employeeRepository.findAll();

        sut.deleteCarById(1L);

        assertThat(carsBeforeDeleteCar.size(), is(equalTo(1)));
        assertThat(employeesBeforeDeleteCar.size(), is(equalTo(1)));
        assertThat(rentsBeforeDeleteCar.size(), is((equalTo(3))));
        assertThat(carRepository.findAll().size(), is(equalTo(0)));
        assertThat(rentRepository.findAll().size(), is(equalTo(1)));
        assertThat(employeeRepository.findAll().size(), is(equalTo(1)));
    }


    @Test
    @Transactional
    public void shouldFindCarsByRentsCount(){
        RentTO rentTO = new RentTO.RentToBuilder().withTakeDate(LocalDate.now()).withReturnDate(LocalDate.now())
                .withCost(100).build();
        RentTO rentTO1 = new RentTO.RentToBuilder().withTakeDate(LocalDate.now()).withReturnDate(LocalDate.now())
                .withCost(101).build();
        RentTO rentTO2 = new RentTO.RentToBuilder().withTakeDate(LocalDate.now()).withReturnDate(LocalDate.now())
                .withCost(102).build();
        RentTO rentTO3 = new RentTO.RentToBuilder().withTakeDate(LocalDate.now()).withReturnDate(LocalDate.now())
                .withCost(103).build();
        List<RentTO> rents = new LinkedList<>();
        rents.add(rentTO);
        rents.add(rentTO1);
        rents.add(rentTO2);
        rents.add(rentTO3);

        CarTO carTO = buildCar(CarType.HATCHBACK, "Opel",rents);
        carRepository.save(CarMapper.toCarEntity(carTO));

        List<RentEntity> savedRents = rentRepository.findAll();

        saveCustomerEntity("Adam", savedRents.subList(0,1));
        saveCustomerEntity("Bartek", savedRents.subList(1,2));
        saveCustomerEntity("Czesław", savedRents.subList(2,4));

        //when
        List<CarTO> findCars = sut.findCarsRentMoreThanXCustomers(2L);
        List<CarTO> noneCars = sut.findCarsRentMoreThanXCustomers(3L);

        assertThat(findCars.size(),is(equalTo(1)));
        assertThat(noneCars.isEmpty(),is(equalTo(true)));
    }

    @Test
    @Transactional
    public void findCarsRentInDateRange(){
        RentTO rentTO = new RentTO.RentToBuilder().withTakeDate(LocalDate.of(2018,1,1))
                .withReturnDate(LocalDate.of(2018,2,10)).withCost(100).build();
        RentTO rentTO1 = new RentTO.RentToBuilder().withTakeDate(LocalDate.of(2018,1,2))
                .withReturnDate(LocalDate.of(2018,7,10)).withCost(101).build();
        RentTO rentTO2 = new RentTO.RentToBuilder().withTakeDate(LocalDate.of(2018,2,10))
                .withReturnDate(LocalDate.of(2018,2,13)).withCost(100).build();
        List<RentTO> rents = new LinkedList<>();
        rents.add(rentTO);
        rents.add(rentTO2);
        List<RentTO> rents1 = new LinkedList<>();
        rents1.add(rentTO1);


        carRepository.save(CarMapper.toCarEntity(buildCar(CarType.HATCHBACK, "Opel",rents)));
        carRepository.save(CarMapper.toCarEntity(buildCar(CarType.SEDAN, "Audi",rents1)));

        LocalDate from = LocalDate.of(2018,2,1);
        LocalDate to = LocalDate.of(2018,3,1);

        //when
        Long countCars= sut.countUsedCarsInDateRange(from, to);

        assertThat(countCars, is(equalTo(2L)));
    }

    private CarTO buildCar(CarType carType, String brand){
        return new CarTO.CarTOBuilder().withCarType(carType).withBrand(brand).withMillage(1).withColor("black")
                .withEngCap(1900).withHorsePower(110).build();
    }
    private CarTO buildCar(CarType carType, String brand, List<RentTO> rentTOs){
        return new CarTO.CarTOBuilder().withCarType(carType).withBrand(brand).withMillage(1).withColor("black")
                .withEngCap(1900).withHorsePower(110).withRents(rentTOs).build();
    }

    public EmployeeTO buildEmployee(){
        PersonalData personalData = new PersonalData("Adam", "Nowak", LocalDate.now());
        return new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData)
                .withPost(Post.ACCAUNTANT).build();
    }

    public EmployeeTO buildEmployee(RentTO rentTO){
        PersonalData personalData = new PersonalData("Adam", "Nowak", LocalDate.now());
        return new EmployeeTO.EmployeeTOBuilder().withPersonalData(personalData)
                .withPost(Post.ACCAUNTANT).build();
    }

    public void saveCustomerEntity(String name, List<RentEntity> rents){
        PersonalData personalData = new PersonalData(name, "Nowak", LocalDate.now());
        AddressData addressData = new AddressData("Wrocław", "Sztabowa", "12A");
        ContactData contactData = new ContactData("+48123456789", "e.m@email.pl");
        CustomerEntity customerEntity = new CustomerEntity("321",addressData,contactData,personalData,rents);
        customerRepository.save(customerEntity);
    }
}